#!../../bin/linux-x86/sendmail

## You may have to change sendmail to something else
## everywhere it appears in this file

< envPaths

epicsEnvSet("STREAM_PROTOCOL_PATH", "$(TOP)/protocols")

cd ${TOP}

## Register all support components
dbLoadDatabase "dbd/sendmail.dbd"
sendmail_registerRecordDeviceDriver pdbbase

drvAsynSerialPortConfigure("null","/dev/null",0,0,0)

## Load record instances
dbLoadRecords("db/sendmail.db","ADDR=epics@gsi.de, device=null")

cd ${TOP}/iocBoot/${IOC}
iocInit

## Start any sequence programs
#seq sncxxx,"user=scsHost"
